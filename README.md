 API
=====

User
--------

![image](REPORT/userAPI.png)

Recipe
--------

![image](REPORT/recipeget.png)
![image](REPORT/recipe_post.png)
![image](REPORT/recipe_delete.png)

Recipe_image
--------

![image](REPORT/image.png)

Favourite
--------

![image](REPORT/favourite.png)

Comment
--------

![image](REPORT/commentpostget.png)
![image](REPORT/commentvoiceputdel.png)