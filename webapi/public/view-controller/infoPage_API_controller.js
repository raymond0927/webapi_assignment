 // API Function 
function getAPI(URLs,submitData,callback){
    console.log(JSON.stringify(submitData));
    $.ajax({
        url: URLs,
        type: "GET",
        data: submitData,                
        success: function ( data , textStatus, xhr) {
            callback({result: true,statusCode: xhr.status , message:data});
        },
        error:function(xhr, textStatus, errorThrown){
            var err = JSON.parse(xhr.responseText);
            callback({result:false,statusCode: xhr.status , message:err.message});
        }
    });
}

function callAPI(type,URLs,submitData,callback){
    console.log(JSON.stringify(submitData));
    $.ajax({
        url: URLs,
        type: type,
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(submitData),                      
        success: function ( data , textStatus, xhr) {
            callback({result: true,statusCode: xhr.status , message:data});
        },
        error:function(xhr, textStatus, errorThrown){
            var err = JSON.parse(xhr.responseText);
            callback({result:false,statusCode: xhr.status , message:err.message});
        }
    });
}

// get Data

function getRecipeById(){
    var URLs="/api/recipe/getRecipeById";
    var submitData = {  
        token: window.sessionStorage.accessToken,
        recipeId:id
    } 
    console.log(JSON.stringify(submitData));
    getAPI(URLs,submitData,function(response) {
        if (response.result){
            console.log("data "+response.result)
            console.log("data "+response.message.message[0].methods_eng)
            Recipeinfo =  response.message.message[0];
            $("#content_title").text(response.message.message[0].title_eng+"      by  "+response.message.message[0].author);
            $("#content_Ingredients").text(response.message.message[0].ingredient_eng);
            $("#content_Methods").text(response.message.message[0].methods_eng);
            if (window.sessionStorage.userId != Recipeinfo.user_id){
                $('#btn_edit').hide();
                $('#btn_delete').hide();
            }
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}
function getimageByid(){
    var URLs="/api/recipe_image/getImageByRecipeid";
    var submitData = {  
        token: window.sessionStorage.accessToken,
        recipeId:id
    } 
    console.log(JSON.stringify(submitData));
    getAPI(URLs,submitData,function(response) {
        if (response.result){
            imageList = response.message.message;
            for(i = 0 ; i<response.message.message.length;i++){
                if (i==0){
                    $('#carousel_inner').append("<div class=\"carousel-item active\"><img src=\""+response.message.message[i].image_path+"\" alt=\"\" class=\"d-block w-100\"></div>");
                }else{
                    $('#carousel_inner').append("<div class=\"carousel-item\"><img src=\""+response.message.message[i].image_path+"\" alt=\"\" class=\"d-block w-100\"></div>");
                }
            }
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}
function getCommentByid(){
    var URLs="/api/comment/getCommentByRecipeId";
    var submitData = {  
        token: window.sessionStorage.accessToken,
        recipeId:id
    } 
    console.log(JSON.stringify(submitData));
    getAPI(URLs,submitData,function(response) {
        if (response.result){
            console.log("getCommentByid data "+response.message.message.length);
            for(i = 0 ; i<response.message.message.length;i++){
                if (response.message.message[i].isVoice == 0){
                     if (response.message.message[i].user_id == window.sessionStorage.userId ){
                        $('#commentList').append("<li class='media'><div id="+response.message.message[i].id+" class='btn-group-vertical'><img src='https://s3.eu-west-2.amazonaws.com/testing-images-api/member.png' alt='Generic placeholder image' class='mr-3 imageCommentIcon'><button id='btn_edit_comment' type='button' align='center'  class='btn btn-outline-primary' onclick='btn_edit_comment_click(this)' data-toggle='modal' data-target='#modalCommentForm'>Edit</button></div><div class='media-body' style='padding:10px'><h3>"+response.message.message[i].author+"</h3><p align='left'>"+response.message.message[i].comment_eng+"</p></div></li>");
                    }else{
                        $('#commentList').append("<li class='media'><img src='https://s3.eu-west-2.amazonaws.com/testing-images-api/member.png' alt='Generic placeholder image' class='mr-3 imageCommentIcon'><div class='media-body' style='padding:10px'><h3>"+response.message.message[i].author+"</h3><p align='left'>"+response.message.message[i].comment_eng+"</p></div></li>");
                    }
                }
            }
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}

// submit Comment
function SubmitComment(comment){
    console.log("SubmitaddRecipe");
    var URLs="/api/comment/createComment?token="+window.sessionStorage.accessToken;
    var submitData = {  
        recipeId: id,  
        comment_eng: comment
    } 
    console.log(JSON.stringify(submitData));
    callAPI('POST',URLs,submitData,function(response) {
        if (response.result){
            alert("Create success ");
            window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}

// like manage
function addLike(recipe_id){
    var URLs="/api/favourite/createFavourite?token="+window.sessionStorage.accessToken;
    var submitData = {  
        recipeId: recipe_id
    } 
    console.log(JSON.stringify(submitData));
    callAPI('POST',URLs,submitData,function(response) {
        if (response.result){
            console.log("success like")
            isLike = true;
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}
function deleteLike(recipe_id){
    var URLs="/api/favourite/deleteFavourite?token="+window.sessionStorage.accessToken;
    var submitData = {  
        recipeId: recipe_id
    } 
    console.log(JSON.stringify(submitData));
    callAPI('delete',URLs,submitData,function(response) {
        if (response.result){
            console.log("success del like")
            isLike = false;
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}

// PUT Recipe

function EditImageCheck(){
    console.log ("imageList "+imageList.length);
    if (imageChangeList.upload1.length==0 && imageChangeList.upload2.length==0 && imageChangeList.upload3.length==0){
        window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
        alert('please select at least one image')
    }else{
        var inputList = new FormData();
        var deleteList =[];
        var image1 = $("#upload_file1")[0].files[0];
        var image2 = $("#upload_file2")[0].files[0];
        var image3 = $("#upload_file3")[0].files[0];
            if (image1!=null){
                inputList.append('image',image1);         
            }
            if (image2!=null){
                inputList.append('image',image2);         
            }
            if (image3!=null){
                inputList.append('image',image3);         
            }
        if (imageChangeList.upload1.length>0){
            if (imageChangeList.upload1 != imageList[0].image_path){
                console.log('put1');
                deleteList.push(imageList[0].id);
            }
        }else{
            console.log('delete1');
            deleteList.push(imageList[0].id);
        }
    

        if (imageChangeList.upload2.length>0){
            if (imageList[1] !=null){
                if (imageChangeList.upload2 != imageList[1].image_path){
                    console.log('put2');
                    deleteList.push(imageList[1].id);
                    if (image2!=null){
                        inputList.append('image',image2);         
                    }
                }
            }
        }else{
            if (imageList[1] !=null){
                console.log('del2');
                deleteList.push(imageList[1].id);
            }
        }
    
        if (imageChangeList.upload3.length>0){
            if (imageList[2] !=null){
                if (imageChangeList.upload3 != imageList[2].image_path){
                    console.log('put3');
                    deleteList.push(imageList[2].id);
                }
            }
        }else{
            if (imageList[2] !=null){
                console.log('del3');
                deleteList.push(imageList[2].id);
            }
        }

        console.log("deleteList "+deleteList.length);
        console.log("inputList "+inputList.length);

        DelImage(deleteList,inputList)
    }
}

function SubmittblImage(fd,recipeId){
        console.log("recipeId "+recipeId);
        var URLs="/api/recipe_image/createImage?token="+window.sessionStorage.accessToken+"&recipeId="+recipeId;

        $.ajax({
            url: URLs,
            type: 'POST',
            contentType:false,
            data: fd,
            processData: false,                
            success: function ( data ) {
                alert("Update success ");
                window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
            },
            error:function(XMLHttpRequest, textStatus, errorThrown){
                var err = JSON.parse(XMLHttpRequest.responseText);
                if (err.message == "Auth failed"){
                    alert("login expired");
                    window.location = "http://"+$(location).attr('host');
                }else{
                    alert(err.message);
                    window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
                }
            }
        });
}

function DelImage(deleteList,inputList){
    if (deleteList.length>0){
            var URLs="/api/recipe_image/deleteImage?token="+window.sessionStorage.accessToken;
            var submitData = {  
                imageId: deleteList
            } 
            console.log(JSON.stringify(submitData));
            callAPI('DELETE',URLs,submitData,function(response) {
                if (response.result){
                    SubmittblImage(inputList,id);
                }else{
                    if (response.message == "Auth failed"){
                        alert("login expired");
                        window.location = "http://"+$(location).attr('host');
                    }else{
                        alert(response.message);
                        window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
                    }
                }
            });
    }else{
        SubmittblImage(inputList,id);
    }
}
function PutRecipe(title,ingredient,methods){
    console.log("SubmitaddRecipe");
    var URLs="/api/recipe/editRecipe?token="+window.sessionStorage.accessToken;
    var submitData = {  
        recipeId: id,  
        title_eng: title,
        ingredient_eng: ingredient,
        methods_eng : methods
    } 
    console.log(JSON.stringify(submitData));
    callAPI('PUT',URLs,submitData,function(response) {
        if (response.result){
            EditImageCheck();
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
                window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
            }
        }
    });
}

// PUT Comment
function PutComment(commentId,comment){
    console.log("SubmitaddRecipe");
    var URLs="/api/comment/editComment?token="+window.sessionStorage.accessToken;
    var submitData = {  
        commentid: commentId,  
        comment_eng: comment
    } 
    console.log(JSON.stringify(submitData));
    callAPI('PUT',URLs,submitData,function(response) {
        if (response.result){
            window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
                window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
            }
        }
    });
}

// Delete Recipe
function DelRecipe(recipeId){
    console.log(" delete!!!!!  "+recipeId)
    var URLs="/api/recipe/deleteRecipe?token="+window.sessionStorage.accessToken;
    var submitData = {  
        recipeId: recipeId
    } 
    console.log(JSON.stringify(submitData));
    callAPI('DELETE',URLs,submitData,function(response) {
        if (response.result){
            window.location = "http://"+$(location).attr('host')+"/main";
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
                window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
            }
        }
    });
}

// Delete Comment

function DelComment(commentId){
    console.log(" delete!!!!!  "+commentId)
    var URLs="/api/comment/deleteComment?token="+window.sessionStorage.accessToken;
    var submitData = {  
        commentid: commentId
    } 
    console.log(JSON.stringify(submitData));
    callAPI('DELETE',URLs,submitData,function(response) {
        if (response.result){
            window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
                window.location = "http://"+$(location).attr('host')+"/info/"+id+"?isLike="+isLike;
            }
        }
    });
}
