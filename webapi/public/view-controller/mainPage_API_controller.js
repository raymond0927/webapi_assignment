function getAPI(URLs,submitData,callback){
    console.log(JSON.stringify(submitData));
    $.ajax({
        url: URLs,
        type: "GET",
        data: submitData,                
        success: function ( data , textStatus, xhr) {
            callback({result: true,statusCode: xhr.status , message:data});
        },
        error:function(xhr, textStatus, errorThrown){
            var err = JSON.parse(xhr.responseText);
            callback({result:false,statusCode: xhr.status , message:err.message});
        }
    });
}

function callAPI(type,URLs,submitData,callback){
    console.log(JSON.stringify(submitData));
    $.ajax({
        url: URLs,
        type: type,
        dataType:'json',
        contentType:"application/json",
        data: JSON.stringify(submitData),                      
        success: function ( data , textStatus, xhr) {
            callback({result: true,statusCode: xhr.status , message:data});
        },
        error:function(xhr, textStatus, errorThrown){
            var err = JSON.parse(xhr.responseText);
            callback({result:false,statusCode: xhr.status , message:err.message});
        }
    });
}


function uploadRecipe(){
    console.log("SubmitaddRecipe");
    var URLs="/api/recipe/createRecipe?token="+window.sessionStorage.accessToken;
    var submitData = {  
        title_eng: $("#inputTitle").val(),  
        ingredient_eng: $("#inputIngredient").val(),
        methods_eng: $("#inputMethod").val()
    } 
    console.log(JSON.stringify(submitData));
    callAPI('POST',URLs,submitData,function(response) {
        if (response.result){
            SubmittblImage(response.message.id)
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}

function SubmittblImage(recipeId){
    console.log("recipeId "+recipeId);
    var URLs="/api/recipe_image/createImage?token="+window.sessionStorage.accessToken+"&recipeId="+recipeId;
    var fd = new FormData();
    var image1 = $("#upload_file1")[0].files[0];
    var image2 = $("#upload_file2")[0].files[0];
    var image3 = $("#upload_file3")[0].files[0];
    if (image1!=null){
        fd.append('image',image1);         
    }
    if (image2!=null){
        fd.append('image',image2);         
    }
    if (image3!=null){
        fd.append('image',image3);         
    }

    $.ajax({
        url: URLs,
        type: 'POST',
        contentType:false,
        data: fd,
        processData: false,                
        success: function ( data ) {
            alert("Create success ");
            window.location = "http://"+$(location).attr('host')+"/main";
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
            var err = JSON.parse(XMLHttpRequest.responseText);
            if (err.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(err.message);
                window.location = "http://"+$(location).attr('host')+"/main/";
            }
        }
    });
}

function getFavouriteListByUserID(){
    var URLs="/api/favourite/getFavouriteListByUserID";
    var submitData = {  
        token: window.sessionStorage.accessToken   
    }  
    getAPI(URLs,submitData,function(response) {
        if (response.result){
            favouriteList = response.message.message;
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}
function getAllRecipe(){
    var URLs="/api/recipe/getAllRecipe";
    var submitData = {  
        token: window.sessionStorage.accessToken   
    } 
    console.log(JSON.stringify(submitData));
    getAPI(URLs,submitData,function(response) {
        if (response.result){
            console.log("data "+response.result)
            try{
                for ( i =0; i <response.message.message.length ; i++){
                    var btn_like_text = "Like";
                    for (y=0;y<favouriteList.length;y++){
                        //- console.log("RAymond123  "+y+"  "+favouriteList[y].recipe_id);
                        if (favouriteList[y].recipe_id == response.message.message[i].id){
                            btn_like_text = " You liked this";
                        }
                    }
                    //- console.log("data "+data.message[i].image_path)
                    $('#recipeList').append("<div id="+response.message.message[i].id+" style=\"width: 18rem;\"  class=\"card\"><img src=\""+response.message.message[i].image_path+"\" alt=\"Card image cap\" class=\"imgCard card-img-top\" onclick=\"goToInfo(this)\"><div class=\"card-body\"><h5 class=\"card-title\">"+response.message.message[i].title_eng+"</h5><div class=\"like-content\"><button class=\"btn-secondary like-review\" onclick=\"likeOnclick(this)\"><i class=\"fa fa-heart\" aria-hidden=\"true\" ></i>"+ btn_like_text+"</button></div></div></div>");
                }
            }catch (err){
                getFavouriteListByUserID();
                getAllRecipe();
            }
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}

function getFavouriteRecipe(){
    var URLs="/api/recipe/getAllRecipe";
    var submitData = {  
        token: window.sessionStorage.accessToken   
    } 
    console.log(JSON.stringify(submitData));
    getAPI(URLs,submitData,function(response) {
        if (response.result){
            console.log("data "+response.result)
            try{
                for ( i =0; i <response.message.message.length ; i++){
                    var btn_like_text = " You liked this";;
                    for (y=0;y<favouriteList.length;y++){
                        //- console.log("RAymond123  "+y+"  "+favouriteList[y].recipe_id);
                        if (favouriteList[y].recipe_id == response.message.message[i].id){
                              $('#recipeList').append("<div id="+response.message.message[i].id+" style=\"width: 18rem;\"  class=\"card\"><img src=\""+response.message.message[i].image_path+"\" alt=\"Card image cap\" class=\"imgCard card-img-top\" onclick=\"goToInfo(this)\"><div class=\"card-body\"><h5 class=\"card-title\">"+response.message.message[i].title_eng+"</h5><div class=\"like-content\"><button class=\"btn-secondary like-review\" onclick=\"likeOnclick(this)\"><i class=\"fa fa-heart\" aria-hidden=\"true\" ></i>"+ btn_like_text+"</button></div></div></div>");
                        }
                    }
                }
            }catch (err){
                getFavouriteListByUserID();
                getFavouriteRecipe();
            }
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}



function getUserRecipe(){
    var URLs="/api/recipe/getRecipeByUserId";
    var submitData = {  
        token: window.sessionStorage.accessToken   
    } 
    console.log(JSON.stringify(submitData));
    getAPI(URLs,submitData,function(response) {
        if (response.result){
            console.log("data "+response.result)
            try{
                for ( i =0; i <response.message.message.length ; i++){
                    var btn_like_text = "Like";
                    for (y=0;y<favouriteList.length;y++){
                        //- console.log("RAymond123  "+y+"  "+favouriteList[y].recipe_id);
                        if (favouriteList[y].recipe_id == response.message.message[i].id){
                            btn_like_text = " You liked this";
                        }
                    }
                    //- console.log("data "+data.message[i].image_path)
                    $('#recipeList').append("<div id="+response.message.message[i].id+" style=\"width: 18rem;\"  class=\"card\"><img src=\""+response.message.message[i].image_path+"\" alt=\"Card image cap\" class=\"imgCard card-img-top\" onclick=\"goToInfo(this)\"><div class=\"card-body\"><h5 class=\"card-title\">"+response.message.message[i].title_eng+"</h5><div class=\"like-content\"><button class=\"btn-secondary like-review\" onclick=\"likeOnclick(this)\"><i class=\"fa fa-heart\" aria-hidden=\"true\" ></i>"+ btn_like_text+"</button></div></div></div>");
                }
            }catch (err){
                getFavouriteListByUserID();
                getUserRecipe();
            }
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}


function addLike(recipe_id){
    var URLs="/api/favourite/createFavourite?token="+window.sessionStorage.accessToken;
    var submitData = {  
        recipeId: recipe_id
    } 
    console.log(JSON.stringify(submitData));
    callAPI('POST',URLs,submitData,function(response) {
        if (response.result){
            console.log("success like")
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}
function deleteLike(recipe_id){
    var URLs="/api/favourite/deleteFavourite?token="+window.sessionStorage.accessToken;
    var submitData = {  
        recipeId: recipe_id
    } 
    console.log(JSON.stringify(submitData));
    callAPI('delete',URLs,submitData,function(response) {
        if (response.result){
            console.log("success del like")
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
            }
        }
    });
}

// PUT User
function PutUser(){
    console.log("SubmitaddRecipe");
    var URLs="/api/users/modifyPassword";
    var submitData = {  
        username: window.sessionStorage.username,  
        password: $("#inputoldPassword").val(),
        newPassword: $("#inputnewPassword").val(),
        newEmail: $("#inputnewEmail").val()
    } 
    console.log(JSON.stringify(submitData));
    callAPI('PUT',URLs,submitData,function(response) {
        if (response.result){
            window.location = "http://"+$(location).attr('host');
        }else{
            if (response.message == "Auth failed"){
                alert("login expired");
                window.location = "http://"+$(location).attr('host');
            }else{
                alert(response.message);
                window.location = "http://"+$(location).attr('host')+"/main/";
            }
        }
    });
}