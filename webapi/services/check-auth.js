const jwt = require('jsonwebtoken');
var config = require('../config.json');

module.exports = (req,res,next)=>{
    try{
        console.log("token : "+req.query.token);

        const decoded = jwt.verify(req.query.token,config.env.JWT_KEY);
         req.userData = decoded;
        next();
    }catch(err){
        return res.status(401).json({Result: false,message:'Auth failed'});
    }


};