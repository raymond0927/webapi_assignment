const sql = require('../services/db.js');

module.exports = {
    createRecipe,
    getAllRecipe,
    getRecipeInfoByid,
    getRecipeInfoByUserid,
    editRecipe,
    deleteRecipe
}; 

// CREATE

function createRecipe(req, callback){

    if (req.body.title_eng == null ||req.body.title_eng.length <1){
        return callback({statusCode:400,result: false,message:'English title is empty'});
    }
  
    if (req.body.ingredient_eng == null ||req.body.ingredient_eng.length <1){
        return callback({statusCode:400,result: false,message:'English ingredient is empty'});
    }

    if (req.body.methods_eng == null ||req.body.methods_eng.length <1){
        return callback({statusCode:400,result: false,message:'English method is empty'});
    }

    // console.log("userData = "+req.userData.userid);

    const queryString = "INSERT INTO `tbl_recipe` (`user_id`, `title_chi`, `title_eng`, `ingredient_chi`, `ingredient_eng`, `methods_chi`, `methods_eng`, `created_date`, `modified_date` , `author`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?);";

    sql.query(queryString,[req.userData.userid,req.body.title_chi,req.body.title_eng,req.body.ingredient_chi,req.body.ingredient_eng,req.body.methods_chi,req.body.methods_eng,new Date(),new Date(),req.userData.username], function(err, rows){
        if (err){ 
          console.log("Fail" +err);
          return callback({statusCode:500,result: false,message:err});
        }
        return callback({statusCode:201,result: true, message:'Create Recipe Success',id:rows.insertId});
  });
}

// GET
function getAllRecipe(req, callback){
    const queryString = "SELECT  tbl_recipe.id,`user_id`, `title_chi`, `title_eng`, tbl_recipe_image.image_path  FROM tbl_recipe LEFT JOIN tbl_recipe_image ON tbl_recipe.id = tbl_recipe_image.recipe_id Group by tbl_recipe.id ;";
    sql.query(queryString, function(err, rows){
          if (err){ 
            console.log("Fail" +err);
            return callback({statusCode:500,result: false,message:err});
          }
            return callback({statusCode:200,result: true, message:rows});
    });
  }

  function getRecipeInfoByid(req, callback){
    //  console.log("getRecipeInfoByid "+req.query.recipeId);
    var recipeId = req.query.recipeId ;
    if (req.query.recipeId != null ){
      recipeId = req.query.recipeId;
    }else if (req.body.recipeId != null ){
      recipeId = req.body.recipeId;
    }else{
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }
    // console.log("getRecipeInfoByid 2");
    // console.log("getRecipeInfoByid recipeId" +recipeId);
    const queryString = "SELECT *  FROM tbl_recipe WHERE id = ?;";
    sql.query(queryString,[recipeId], function(err, rows){
          if (err){ 
            console.log("Fail" +err);
            return callback({statusCode:500,result: false,message:err});
          }
            return callback({statusCode:200,result: true, message:rows});
    });
  }

  function getRecipeInfoByUserid(req, callback){
    const queryString = "SELECT  tbl_recipe.id,`user_id`, `title_chi`, `title_eng`, tbl_recipe_image.image_path  FROM tbl_recipe LEFT JOIN tbl_recipe_image ON tbl_recipe.id = tbl_recipe_image.recipe_id Group by tbl_recipe.id Having user_id= ? ;";
    sql.query(queryString,[req.userData.userid], function(err, rows){
          if (err){ 
            console.log("Fail" +err);
            return callback({statusCode:500,result: false,message:err});
          }
            return callback({statusCode:200,result: true, message:rows});
    });
  }
  // EDIT 

  function editRecipe(req, callback){

    if (req.body.recipeId == null ||req.body.recipeId.length <1){
        return callback({statusCode:400,result: false,message:'Recipe id is empty'});
    }

    // console.log("userData = "+req.userData.userid);

    const queryString = "UPDATE `tbl_recipe` SET `title_chi` =?, `title_eng` = ?, `ingredient_chi` = ?, `ingredient_eng` = ?, `methods_chi` = ?, `methods_eng` = ? ,`modified_date` =? WHERE (`id` = ?); ";

    getRecipeInfoByid(req, function(result){
      // console.log("userData = "+result.message);
      if (result.message.length <1 || !result.result){
        return callback({statusCode:401,result: false,message:'Edit Fail'});
      }
      // console.log("result.message.user_id "+result.message[0].user_id);
      if (result.message[0].user_id != req.userData.userid){
        return callback({statusCode:401,result: false,message:'Edit Fail, permission denied'});
      }

      var title_chi = result.message[0].title_chi;
      var title_eng = result.message[0].title_eng;
      var ingredient_chi = result.message[0].ingredient_chi;
      var ingredient_eng = result.message[0].ingredient_eng;
      var methods_chi = result.message[0].methods_chi;
      var methods_eng = result.message[0].methods_eng; 

      if (req.body.title_chi != null ){
        title_chi = req.body.title_chi
      }

      if (req.body.title_eng != null ){
        title_eng = req.body.title_eng
      }

      if (req.body.ingredient_chi != null ){
        ingredient_chi = req.body.ingredient_chi
      }

      if (req.body.ingredient_eng != null ){
        ingredient_eng = req.body.ingredient_eng
      }

      if (req.body.methods_chi != null ){
        methods_chi = req.body.methods_chi
      }

      if (req.body.methods_eng != null ){
        methods_eng = req.body.methods_eng
      }

      sql.query(queryString,[title_chi,title_eng,ingredient_chi,ingredient_eng,methods_chi,methods_eng,new Date(),req.body.recipeId], function(err, rows){
        if (err){ 
          console.log("Fail" +err);
          return callback({statusCode:500,result: false,message:err});
        }
        return callback({statusCode:201,result: true, message:'Edit Recipe Success'});
  });

   });
}

// Delete 


function deleteRecipe(req, callback){

  if (req.body.recipeId == null ||req.body.recipeId.length <1){
      return callback({statusCode:400,result: false,message:'Recipe id is empty'});
  }

  // console.log("userData = "+req.userData.userid);

  const queryString = "DELETE FROM `tbl_recipe` WHERE (`id` = ?);";

  getRecipeInfoByid(req, function(result){
    // console.log("userData = "+result.message);
    if (result.message.length <1 || !result.result){
      return callback({statusCode:401,result: false,message:'Delete Fail'});
    }
    // console.log("result.message.user_id "+result.message[0].user_id);
    if (result.message[0].user_id != req.userData.userid){
      return callback({statusCode:401,result: false,message:'Delete Fail, permission denied'});
    }

    sql.query(queryString,[req.body.recipeId], function(err, rows){
      if (err){ 
        console.log("Fail" +err);
        return callback({statusCode:500,result: false,message:err});
      }
      return callback({statusCode:201,result: true, message:'Delete Recipe Success'});
});

 });
}