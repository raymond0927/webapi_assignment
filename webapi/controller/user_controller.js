const sql = require('../services/db.js');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var config = require('../config.json');

module.exports = {
    createUser,
    loginChecking,
    modifyUser
}; 


 function createUser(req, callback){
    if (req.body.email == null ||req.body.email.length <1){
        return callback({statusCode:400,result: false,message:'email is empty'});
    }
  
    if (req.body.username == null || req.body.password == null ||req.body.username.length <1 || req.body.password.length <1){
      return callback({statusCode:400,result: false,message:'username/password is empty'});
    }
    const email = req.body.email;
    const username = req.body.username;
    const password = bcrypt.hashSync(req.body.password, 10);
    const queryString = "INSERT INTO `tbl_user` (`username`, `password`,`email`,`created_date`,`modified_date`) VALUES (?, ?,?,?,?);";

    getUserCount(req.body.username, function(result){
        if (result[0].total == 0){
            sql.query(queryString,[username,password,email,new Date(),new Date()],(err,rows)=>{
                if (err){
                  console.log("Fail" +err);
                  return callback({statusCode:500,result: false,message:err});
                }
                // console.log("create new user ",rows);
                return callback({statusCode:201,result: true, message:'Sign up Success'});
              })
        }else{
            return callback({statusCode:400,result: false,message:'User name already exist.'});
        }
     });
}


function loginChecking(req, callback){
  if (req.body.username == null || req.body.password == null ||req.body.username.length <1 || req.body.password.length <1){
    return callback({statusCode:400,result: false,message:'username/password is empty'});
  }
  const username = req.body.username;
  const password = req.body.password;

  getUserInfo(username, function(result){
    if (result.row.length <1 || !result.result){
      return callback({statusCode:401,result: false,message:'Invaild User name / password'});
    }
    // console.log("login Check " + result.row[0].password);
    // console.log(req.body.password);
    // console.log(config.env.JWT_KEY);
    const userId = result.row[0].id ;
    bcrypt.compare(password,result.row[0].password,(err,result)=>{
        if (err){
            return callback({statusCode:401,result: false,message:'Invaild User name / password'});
        }

        if (result){
            const token = jwt.sign({
               username : username,
               userid : userId
            },config.env.JWT_KEY,
            {
                expiresIn : "1h"
            });

          return callback({statusCode:200,result: true, message:'Login Successful',token :token, userId :userId });
        }else{
          return callback({statusCode:401,result: false,message:'Login failed, Invaild User name / password'});
        }
    });
 });
}


function modifyUser(req, callback){
  if (req.body.username == null || req.body.password == null ||req.body.username.length <1 || req.body.password.length <1){
    return callback({statusCode:400,result: false,message:'username/password is empty'});
  }

  const username = req.body.username;
  const password = req.body.password;
  var newPassword = "";
  var newEmail = "";
  if (req.body.newPassword != null && req.body.newPassword.length >0){
    newPassword = bcrypt.hashSync(req.body.newPassword, 10);
  }
  if (req.body.newEmail != null && req.body.newEmail.length >0){
    newEmail = req.body.newEmail;
  }
  const queryString = "UPDATE `tbl_user` SET  `email`=?, `password` = ? ,`modified_date`= ? WHERE (`id` = ?);";

  getUserInfo(username, function(result){
    if (result.row.length <1 || !result.result){
      return callback({statusCode:401,result: false,message:'Invalid username / password'});
    }
    if (newPassword == null || newPassword.length <1){
      newPassword = result.row[0].password;
    }
    if (newEmail == null || newEmail.length <1){
      newEmail = result.row[0].email;
    }
    // console.log("login Check " + result.row[0].password);
    // console.log(req.body.password);
    // console.log(config.env.JWT_KEY);
    const userId = result.row[0].id ;
    bcrypt.compare(password,result.row[0].password,(err,result)=>{
        if (err){
            return callback({statusCode:401,result: false,message:'Auth failed'});
        }
        if (result){
          sql.query(queryString,[newEmail,newPassword,new Date(),userId],(err,rows)=>{
            if (err){
              console.log("Fail" +err);
              return callback({statusCode:500,result: false,message:err});
            }
            return callback({statusCode:201,result: true, message:'Modified Success'});
          });
        }else{
          console.log("Result : false");
          return callback({statusCode:401,result: false,message:'Invalid username / password'});
        }
    });
 });

}


function getUserCount(username, callback){
  const queryString = "SELECT count(*)as total FROM `tbl_user` where username = ?;";
  sql.query(queryString,[username], function(err, rows){
        if (err){ 
          return callback({statusCode:500,result: false,message:err});
        }
        return callback(rows);
});
}

function getUserInfo(username, callback){
  const queryString = "SELECT * FROM `tbl_user` where username = ?;";
  sql.query(queryString,[username], function(err, rows){
        if (err){ 
          console.log("Fail" +err);
          return callback({statusCode:500,result: false,message:err});
        }
        if (rows.length >0){
          // console.log("getUserInfo  " +rows[0].username);
          return callback({result: true, row:rows});
        }else{
          return callback({result: false, row:rows});
        }

  });
}