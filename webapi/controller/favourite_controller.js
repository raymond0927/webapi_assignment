const sql = require('../services/db.js');
const recipeController = require('../controller/recipe_controller');

module.exports = {
    createFavourite,
    getFavouriteListByUserID,
    deleteFavourite
}; 

// CREATE

function createFavourite(req, callback){

    if (req.body.recipeId == null ||req.body.recipeId.length <1){
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }

    // console.log("userData = "+req.userData.userid);

    const queryString = "INSERT INTO `tbl_favourite` (`user_id`, `recipe_id`, `created_date`, `modified_date`) VALUES (?, ?,?, ?);";

    recipeController.getRecipeInfoByid(req, function(result){
        // console.log("userData = "+result.message);
        if (result.message.length <1 || !result.result){
        return callback({statusCode:401,result: false,message:'Add Favourite Fail'});
        }
        existChecking(req, function(result){
            if (result[0].total == 0){
                sql.query(queryString,[req.userData.userid,req.body.recipeId,new Date(),new Date()], function(err, rows){
                    if (err){ 
                    console.log("Fail" +err);
                    return callback({statusCode:500,result: false,message:err});
                    }
                    return callback({statusCode:201,result: true, message:'Add Favourite Success'});
                });
            }else{
                return callback({statusCode:400,result: false, message:'Already add'});
            }
        });
    });
}

//Delete 

function deleteFavourite(req, callback){

    if (req.body.recipeId == null ||req.body.recipeId.length <1){
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }

    // console.log("userData = "+req.userData.userid);

    const queryString = "DELETE FROM `tbl_favourite` WHERE (`id` = ?);";

    getFavouriteListByid(req, function(result){
        // console.log("userData = "+result.message);
        if (result.message.length <1 || !result.result){
            return callback({statusCode:401,result: false,message:'Delete Fail'}); 
        }
        console.log("result.message.user_id "+result.message[0].user_id);
        if (result.message[0].user_id != req.userData.userid){
            return callback({statusCode:401,result: false,message:'Delete Fail, permission denied'});
        }

        sql.query(queryString,[result.message[0].id], function(err, rows){
            if (err){ 
                console.log("Fail" +err);
                return callback({statusCode:500,result: false,message:err});
            }
            return callback({statusCode:201,result: true, message:'Delete Favourite Success'});
        });

    });
}

//GET

function getFavouriteListByUserID(req, callback){
    const queryString = "SELECT *  FROM `tbl_favourite` where user_id = ? ;";
    sql.query(queryString,[req.userData.userid], function(err, rows){
        if (err){ 
            return callback({statusCode:500,result: false,message:err});
        }
        return callback({statusCode:200,result: true, message:rows});
    });
}

function getFavouriteListByid(req, callback){
    var recipeId = req.query.recipeId ;
    if (req.query.recipeId != null ){
      recipeId = req.query.recipeId;
    }else if (req.body.recipeId != null ){
      recipeId = req.body.recipeId;
    }else{
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }
    const queryString = "SELECT *  FROM `tbl_favourite` where recipe_id = ? AND user_id = ?;";
    sql.query(queryString,[recipeId,req.userData.userid], function(err, rows){
        if (err){ 
            return callback({statusCode:500,result: false,message:err});
        }
        return callback({statusCode:200,result: true, message:rows});
    });
}

function existChecking(req, callback){
    var recipeId = req.query.recipeId ;
    if (req.query.recipeId != null ){
      recipeId = req.query.recipeId;
    }else if (req.body.recipeId != null ){
      recipeId = req.body.recipeId;
    }else{
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }
    const queryString = "SELECT count(*)as total FROM `tbl_favourite` where user_id = ? AND  recipe_id = ? ;";
    sql.query(queryString,[req.userData.userid,recipeId], function(err, rows){
          if (err){ 
            return callback({statusCode:500,result: false,message:err});
          }
          return callback(rows);
  });
}