const sql = require('../services/db.js');
const recipeController = require('./recipe_controller');


module.exports = {
    createComment,
    getCommentByRecipeId,
    editComment,
    deleteComment,
    createVoiceComment,
    InpuChecking,
    getCommentByid
}; 

// CREATE

function createComment(req, callback){

    if (req.body.recipeId == null ||req.body.recipeId.length <1){
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }
  
    if (req.body.comment_eng == null ||req.body.comment_eng.length <1){
        return callback({statusCode:400,result: false,message:'comment_eng is empty'});
    }

    // console.log("userData = "+req.userData.userid);
    // console.log("req.body.recipeId = "+req.body.recipeId);

    const queryString = "INSERT INTO `tbl_comment` (`user_id`, `recipe_id`, `comment_chi`, `comment_eng`, `modified_date` , `created_date`, `author`, `isVoice`) VALUES (?, ?, ?, ?, ?,?,?,?);    ";
    // Check the recipe ID is exist
    recipeController.getRecipeInfoByid(req, function(result){
            console.log("userData = "+result.message);
            if (result.message.length <1 || !result.result){
            return callback({statusCode:401,result: false,message:'Create Fail'});
            }


            sql.query(queryString,[req.userData.userid,req.body.recipeId,req.body.comment_chi,req.body.comment_eng,new Date(),new Date(),req.userData.username,false], function(err, rows){
                if (err){ 
                    console.log("Fail" +err);
                    return callback({statusCode:500,result: false,message:err});
                }
                return callback({statusCode:201,result: true, message:'Create Comment Success',id:rows.insertId});
            });
                
    });  
}

function createVoiceComment(req, callback){
    console.log("userData = "+req.userData.userid);

    const queryString = "INSERT INTO `tbl_comment` (`user_id`, `recipe_id`, `voice_path`,  `modified_date` , `created_date`, `author`, `isVoice`) VALUES (?, ?, ?, ?, ?,?,?);    ";

    // Check the recipe ID is exist
            sql.query(queryString,[req.userData.userid,req.query.recipeId,req.body.voice_path,new Date(),new Date(),req.userData.username,true], function(err, rows){
                if (err){ 
                    console.log("Fail" +err);
                    return callback({statusCode:500,result: false,message:err});
                }
                return callback({statusCode:201,result: true, message:'Create Comment Success'});
            });
}

// PUT

function editComment(req, callback){

    console.log("userData = "+req.userData.userid);

    const queryString = "UPDATE `tbl_comment` SET `comment_chi` = ?, `comment_eng` = ?, `modified_date` = ? WHERE (`id` = ?);";

    getCommentByid(req, function(result){
        // console.log("userData = "+result.message);
        if (result.message.length <1 || !result.result){
            return callback({statusCode:401,result: false,message:'Edit Fail'});
        }
        // console.log("result.message.user_id "+result.message[0].user_id);
        if (result.message[0].user_id != req.userData.userid){
            return callback({statusCode:401,result: false,message:'Edit Fail, permission denied'});
        }

        var comment_chi = result.message[0].comment_chi;
        var comment_eng = result.message[0].comment_eng;

        if (req.body.comment_chi != null ){
            comment_chi = req.body.comment_chi
        }

        if (req.body.comment_eng != null  ){
            if (req.body.comment_eng.length >0){
                comment_eng = req.body.comment_eng
            }
        }

        sql.query(queryString,[comment_chi,comment_eng,new Date(),req.body.commentid], function(err, rows){
            if (err){ 
                console.log("Fail" +err);
                return callback({statusCode:500,result: false,message:err});
            }
            return callback({statusCode:201,result: true, message:'Edit Comment Success'});
        });

    });
}

//Delete

function deleteComment(req, callback){

    // console.log("userData = "+req.userData.userid);

    const queryString = "DELETE FROM `tbl_comment` WHERE (`id` = ?);";

    getCommentByid(req, function(result){
        // console.log("userData = "+result.message);
        if (result.message.length <1 || !result.result){
            return callback({statusCode:401,result: false,message:'Delete Fail'});
        }
        // console.log("result.message.user_id "+result.message[0].user_id);
        if (result.message[0].user_id != req.userData.userid){
            return callback({statusCode:401,result: false,message:'Delete Fail, permission denied'});
        }

        sql.query(queryString,[req.body.commentid], function(err, rows){
            if (err){ 
                console.log("Fail" +err);
                return callback({statusCode:500,result: false,message:err});
            }
            return callback({statusCode:201,result: true, message:'Delete Comment Success'});
        });

    });
}


// GET

function getCommentByRecipeId(req, callback){
    var recipeId = req.query.recipeId ;
    if (req.query.recipeId != null ){
      recipeId = req.query.recipeId;
    }else{
      recipeId = req.body.recipeId;
    }
    // console.log("getCommentByRecipeId recipeId" +recipeId);
    const queryString = "SELECT  *  FROM tbl_comment WHERE recipe_id = ? ;";
    sql.query(queryString,[recipeId], function(err, rows){
          if (err){ 
            console.log("Fail" +err);
            return callback({statusCode:500,result: false,message:err});
          }
            return callback({statusCode:200,result: true, message:rows});
    });
  }

  function getCommentByid(req, callback){
    var commentid = req.query.commentid ;
    if (req.query.commentid != null ){
        commentid = req.query.commentid;
    }else if (req.body.commentid != null) {
        commentid = req.body.commentid;
    }
    // console.log("getCommentByid commentid" +commentid);
    const queryString = "SELECT *  FROM tbl_comment WHERE id = ?;";
    sql.query(queryString,[commentid], function(err, rows){
          if (err){ 
            console.log("Fail" +err);
            return callback({statusCode:500,result: false,message:err});
          }
            return callback({statusCode:200,result: true, message:rows});
    });
  }

  function InpuChecking(req, callback){
    if (req.query.recipeId == null ||req.query.recipeId.length <1){
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }

    // console.log("userData = "+req.userData.userid);

    // Check the recipe ID is exist
    recipeController.getRecipeInfoByid(req, function(result){
            // console.log("userData = "+result.message);
            if (result.message.length <1 || !result.result){
            return callback({statusCode:401,result: false,message:'Create Fail'});
            }

            return callback({statusCode:201,result: true, message:'Able to insert'});
    });  
}