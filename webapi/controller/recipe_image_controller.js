const sql = require('../services/db.js');
const recipeController = require('../controller/recipe_controller');


module.exports = {
    createImage,
    getImageByRecipeid,
    deleteImage,
    InpuChecking,
    delchecking

}; 

// CREATE

function createImage(req, callback){

    if (req.query.recipeId == null ||req.query.recipeId.length <1){
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }

    console.log("userData = "+req.userData.userid);

    const queryString = "INSERT INTO `tbl_recipe_image` (`recipe_id`, `image_path`, `modified_date`, `created_date`) VALUES (?, ?, ?, ?);";
            for (i =0; i<=req.body.image_path.length;i++){
                console.log("result.message.image_path "+i+"  "+req.body.image_path[i]);
                if (req.body.image_path[i] != null){
                    sql.query(queryString,[req.query.recipeId,req.body.image_path[i],new Date(),new Date()], function(err, rows){
                        if (err){ 
                            console.log("Fail" +err);
                            return callback({statusCode:500,result: false,message:err});
                        }
                    });
                }
            }
            return callback({statusCode:201,result: true, message:'Create Image Success'}); 
}

// Delete

function deleteImage(req, callback){

    if (req.body.imageId == null ||req.body.imageId.length <1){
        return callback({statusCode:400,result: false,message:'imageId is empty'});
    }

    const imageId = req.body.imageId;

    const queryString = "DELETE FROM `tbl_recipe_image` WHERE (`id` in (?));";

    delchecking(req, function(result){
            if (result.message.length <1 || !result.result){
            return callback({statusCode:401,result: false, message:result.message});
            }
                sql.query(queryString,[imageId], function(err, rows){
                    if (err){ 
                        console.log("Fail" +err);
                        return callback({statusCode:500,result: false,message:err});
                    }
                    return callback({statusCode:201,result: true, message:"Delete Image Success"});
                });
        });  

}


// GET

function getImageByRecipeid(req, callback){
    var recipeId = req.query.recipeId ;
    if (req.query.recipeId != null ){
      recipeId = req.query.recipeId;
    }else if (req.body.recipeId != null ){
      recipeId = req.body.recipeId;
    }else{
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }
    console.log("getRecipeInfoByid recipeId" +recipeId);
    const queryString = "SELECT *  FROM tbl_recipe_image WHERE recipe_id = ?;";
    sql.query(queryString,[recipeId], function(err, rows){
          if (err){ 
            console.log("Fail" +err);
            return callback({statusCode:500,result: false,message:err});
          }
            return callback({statusCode:200,result: true, message:rows});
    });
  }

function getImageById(req, callback){
    // console.log("body +"+req.body);
    var imageId = req.body.processID ;
    if (req.body.processID != null ){
        imageId = req.body.processID;
    }else{
        return callback({statusCode:400,result: false,message:'imageId is empty'});
    }
    // console.log("getImageById imageId" +imageId);
    const queryString = "SELECT *  FROM tbl_recipe_image WHERE id = ?;";
    sql.query(queryString,[imageId], function(err, rows){
          if (err){ 
            console.log("Fail" +err);
            return callback({statusCode:500,result: false,message:err});
          }
            return callback({statusCode:200,result: true, message:rows});
    });
}


function InpuChecking(req, callback){
    if (req.query.recipeId == null ||req.query.recipeId.length <1){
        return callback({statusCode:400,result: false,message:'recipeId is empty'});
    }

    // console.log("userData = "+req.userData.userid);

    // Check the recipe ID is exist
    recipeController.getRecipeInfoByid(req, function(result){
            // console.log("userData = "+result.message);
            if (result.message.length <1 || !result.result){
            return callback({statusCode:401,result: false,message:'Create Fail'});
            }
            // console.log("result.message.user_id "+result.message[0].user_id);
            if (result.message[0].user_id != req.userData.userid){
            return callback({statusCode:401,result: false,message:'Create Fail, permission denied'});
            }

            return callback({statusCode:201,result: true, message:'Able to insert'});
    });  
}

function delchecking(req, callback){
    const imageId = req.body.imageId;
    var count =0;
    for ( i = 0 ; i < imageId.length; i++){
        // console.log("imageID i "+i+"   "+imageId[i]);
        req.body.processID = imageId[i];
        getImageById(req, function(result){
            // console.log("userData = "+result.message);
            if (result.message.length <1 || !result.result){
            return callback({statusCode:401,result: false,message:'Delete Fail'});
            }
            req.body.recipeId = result.message[0].recipe_id;
            // Check the recipe ID is exist
            recipeController.getRecipeInfoByid(req, function(result){
                // console.log("userData = "+result.message);
                if (result.message.length <1 || !result.result){
                return callback({statusCode:401,result: false,message:'Delete Fail'});
                }
                // console.log("result.message.user_id "+result.message[0].user_id);
                if (result.message[0].user_id != req.userData.userid){
                    return callback({statusCode:401,result: false,message:'Delete Fail, permission denied'});
                }
                count++;
                if (count == imageId.length){
                    return callback({statusCode:200,result: true,message:'Able to delete'});
                }
            });  

        });  
    }
}