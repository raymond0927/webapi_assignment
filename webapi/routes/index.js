var express = require('express');
var router = express.Router();



router.get('/', function(req, res, next) {
    res.render('login')
});

router.get('/main/', function(req, res, next) {
    console.log("req.query.type "+req.query.type);
    res.render('main', {type: req.query.type})
});

router.get('/register', function(req, res, next) {
    res.render('register')
});


router.get('/info/:id', function(req, res, next) {
    res.render('info',{id: req.params.id,isLike:req.query.isLike})
});
module.exports = router;
