var express = require('express');
var router = express.Router();
const recipe_imageController = require('../controller/recipe_image_controller');
const checkAuth = require('../services/check-auth.js');
const upload = require('../services/file-upload');

const UploadImage = upload.array('image');


router.post('/createImage',checkAuth,createImage);
router.get('/getImageByRecipeid',checkAuth,getImageByRecipeid);
router.delete('/deleteImage',checkAuth,deleteImage);

//  createImage

function createImage(req, res, next) {
    console.log("createImage" );
    recipe_imageController.InpuChecking(req, function(result){
      console.log("final ===="+result.result);
      if (!result.result){
        res.statusCode = result.statusCode;
        return res.json({Result: result.result, message:result.message});
      }
      UploadImage(req,res,function(err){
        if (err){
            return res.status(422).send({errors:[{title:'File Upload Error',detail: err.message}]});
        }
        
        console.log(req.files.length)
        var fileList = [];
        for(i=0 ; i<req.files.length;i++){
            fileList.push(req.files[i].location)
        }
        req.body.image_path = fileList;
        recipe_imageController.createImage(req, function(result){
          console.log("final ===="+result.result);
          res.statusCode = result.statusCode;
          return res.json({Result: result.result, message:result.message})
      });
    });
   });
  }

  //getImageByRecipeid 
  function getImageByRecipeid(req, res, next) {
    console.log("getImageByRecipeid" );
    recipe_imageController.getImageByRecipeid(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }

  //deleteImage

  function deleteImage(req, res, next) {
    console.log("getImageByRecipeid" );
    recipe_imageController.deleteImage(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }


  module.exports = router;