var express = require('express');
var router = express.Router();
const recipeController = require('../controller/recipe_controller');
const checkAuth = require('../services/check-auth.js');

router.post('/createRecipe',checkAuth,createRecipe);
router.get('/getAllRecipe',checkAuth,getAllRecipe);
router.get('/getRecipeById',checkAuth,getRecipeInfoByid);
router.get('/getRecipeByUserId',checkAuth,getRecipeInfoByUserid);
router.put('/editRecipe',checkAuth,editRecipe);
router.delete('/deleteRecipe',checkAuth,deleteRecipe);

//  createRecipe

function createRecipe(req, res, next) {
    console.log("createRecipe" );
    recipeController.createRecipe(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message, id :result.id})
   });
  }


  // get function 
  function getAllRecipe(req, res, next) {
    console.log("getAllRecipe" );
    recipeController.getAllRecipe(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }

  function getRecipeInfoByid(req, res, next) {
    console.log("getRecipeInfoByid" );
    recipeController.getRecipeInfoByid(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }

  function getRecipeInfoByUserid(req, res, next) {
    console.log("getRecipeInfoByUserid" );
    recipeController.getRecipeInfoByUserid(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }

  // Edit 

  function editRecipe(req, res, next) {
    console.log("editRecipe" );
    recipeController.editRecipe(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }

  // Delete

  function deleteRecipe(req, res, next) {
    console.log("deleteRecipe" );
    recipeController.deleteRecipe(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }



  module.exports = router;