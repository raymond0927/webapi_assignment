var express = require('express');
var router = express.Router();
const userController = require('../controller/user_controller');
const checkAuth = require('../services/check-auth.js');


router.post('/sign',signUp);
router.post('/login',login);
router.put('/modifyPassword',modifyPassword);

//  sign up

function signUp(req, res, next) {
  console.log("createUser" );
  userController.createUser(req, function(result){
    console.log("final ===="+result.result);
    res.statusCode = result.statusCode;
    return res.json({Result: result.result, message:result.message})
 });
}

// Login

function login(req, res, next) {
  console.log("Login" );
  userController.loginChecking(req, function(result){
    console.log("final ===="+result.result);
    res.statusCode = result.statusCode;
    if (result.statusCode == 200){
      return res.json({Result: result.result, message:result.message,userId :result.userId,token:result.token});
    }else{
          return res.json({Result: result.result, message:result.message});
    }
 });
}

// modify password

function modifyPassword(req, res, next) {
  console.log("modify user" );
  userController.modifyUser(req, function(result){
    console.log("final ===="+result.result);
    res.statusCode = result.statusCode;
    return res.json({Result: result.result, message:result.message});
 });
}


module.exports = router;


