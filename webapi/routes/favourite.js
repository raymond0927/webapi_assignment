var express = require('express');
var router = express.Router();
const favouriteController = require('../controller/favourite_controller');
const checkAuth = require('../services/check-auth.js');

router.post('/createFavourite',checkAuth,createFavourite);
router.get('/getFavouriteListByUserID',checkAuth,getFavouriteListByUserID);
router.delete('/deleteFavourite',checkAuth,deleteFavourite);

//  createFavourite

function createFavourite(req, res, next) {
    console.log("createFavourite" );
    favouriteController.createFavourite(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }

  //  getFavouriteListByUserID

function getFavouriteListByUserID(req, res, next) {
    console.log("getFavouriteListByUserID" );
    favouriteController.getFavouriteListByUserID(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }

  //deleteFavourite

  function deleteFavourite(req, res, next) {
    console.log("deleteFavourite" );
    favouriteController.deleteFavourite(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }

  module.exports = router;