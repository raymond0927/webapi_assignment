var express = require('express');
var router = express.Router();
const commentController = require('../controller/comment_controller');
const checkAuth = require('../services/check-auth.js');
const upload = require('../services/voice-upload');

const UploadVoice = upload.single('voice');

router.post('/createComment',checkAuth,createComment);
router.post('/createVoiceComment',checkAuth,createVoiceComment);
router.put('/editComment',checkAuth,editComment);
router.delete('/deleteComment',checkAuth,deleteComment);
router.get('/getCommentByRecipeId',checkAuth,getCommentByRecipeId);

function createComment(req, res, next) {
    console.log("createComment" );
    commentController.createComment(req, function(result){
      console.log("final ===="+result.result);
      res.statusCode = result.statusCode;
      return res.json({Result: result.result, message:result.message})
   });
  }

function createVoiceComment(req, res, next) {
    console.log("createVoiceComment" );
    
    commentController.InpuChecking(req, function(result){
        console.log("final ===="+result.result);
        if (!result.result){
          res.statusCode = result.statusCode;
          return res.json({Result: result.result, message:result.message});
        }
        UploadVoice(req,res,function(err){
          if (err){
              return res.status(422).send({errors:[{title:'File Upload Error',detail: err.message}]});
          }
          
          console.log(req.file.location)
          req.body.voice_path = req.file.location;
          commentController.createVoiceComment(req, function(result){
              console.log("final ===="+result.result);
              res.statusCode = result.statusCode;
              return res.json({Result: result.result, message:result.message})
          });
      });
     });
}

    // get function 
function getCommentByRecipeId(req, res, next) {
    console.log("getCommentByRecipeId" );
    commentController.getCommentByRecipeId(req, function(result){
        console.log("final ===="+result.result);
        res.statusCode = result.statusCode;
        return res.json({Result: result.result, message:result.message})
    });
}

      // put function 
function editComment(req, res, next) {
    console.log("editComment" );
    commentController.editComment(req, function(result){
        console.log("final ===="+result.result);
        res.statusCode = result.statusCode;
        return res.json({Result: result.result, message:result.message})
    });
}

      // delete function 
    function deleteComment(req, res, next) {
        console.log("deleteComment" );
        commentController.deleteComment(req, function(result){
            console.log("final ===="+result.result);
            res.statusCode = result.statusCode;
            return res.json({Result: result.result, message:result.message})
        });
    }
    

  module.exports = router;