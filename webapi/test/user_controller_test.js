var chai = require('chai');
var expect = chai.expect;
var User = require('../controller/user_controller');
var sinon = require('sinon');

describe('user_controller', function() {
    it('login() should return the list',
        function(done) {
            var req ={
                body:{username:"raymond",password:"12345678"}
            }
            User.loginChecking(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
    });

    it('modifyUser should return boolean to show edit success',
    function(done) {
        var req ={
            body:{username:"raymond",password:"12345678"}
        }
        User.modifyUser(req, function(result){
            expect(result.result).to.eql(true);
            done();
        });  
    });

    it('createUser should return boolean to show create success',
    function(done) {
        var req ={
            body:{username:"testtesttest",password:"12345678",email:"123@email.com"}
        }
        User.createUser(req, function(result){
            expect(result.result).to.eql(true);
            done();
        });  
    });


});