var chai = require('chai');
var expect = chai.expect;
var Favourite = require('../controller/favourite_controller');
var sinon = require('sinon');

describe('favourite_controller', function() {
    it('getFavouriteListByUserID() should return the list',
        function(done) {
            var req ={
                userData:{userid:2}
            }
            Favourite.getFavouriteListByUserID(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
    });

    it('createFavourite() should return boolean to show create success',
        function(done) {
            var req ={
                query:{recipeId:2},
                body:{recipeId:2},
                userData:{userid:2}
            }
            Favourite.createFavourite(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
    });

    it('deleteFavourite() should return boolean to show delete success',
    function(done) {
        var req ={
            query:{recipeId:2},
            body:{recipeId:2},
            userData:{userid:2}
        }
        Favourite.deleteFavourite(req, function(result){
            expect(result.result).to.eql(true);
            done();
        });  
});

});