var chai = require('chai');
var expect = chai.expect;
var Recipe = require('../controller/recipe_controller');
var sinon = require('sinon');

describe('recipe_controller', function() {
    it('getAllRecipe() should return the list',
        function(done) {
            var req ={
                userData:{userid:2}
            }
            Recipe.getAllRecipe(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
    });

    it('getRecipeInfoByid() should return the list',
    function(done) {
        var req ={
            query:{recipeId:2},
            userData:{userid:2}
        }
        Recipe.getRecipeInfoByid(req, function(result){
            expect(result.result).to.eql(true);
            done();
        });  
    });

    it('getRecipeInfoByUserid() should return the user created list',
    function(done) {
        var req ={
            query:{recipeId:2},
            userData:{userid:2}
        }
        Recipe.getRecipeInfoByUserid(req, function(result){
            expect(result.result).to.eql(true);
            done();
        });  
    });

    it('editRecipe() should return boolean to show edit success',
    function(done) {
        var req ={
            query:{recipeId:2},
            body:{recipeId:2},
            userData:{userid:2}
        }
        Recipe.editRecipe(req, function(result){
            expect(result.result).to.eql(true);
            done();
        });  
    });

    it('createRecipe() should return boolean to show create success',
    function(done) {
        var req ={
            body:{title_eng:"test",ingredient_eng:"ingredient_eng",methods_eng:"methods_eng"},
            userData:{userid:2,username:"raymond"}
        }
        Recipe.createRecipe(req, function(result){
            expect(result.result).to.eql(true);
            done();
        });  
    });


    it('deleteRecipe() should return boolean to show delete success',
    function(done) {
        var req ={
            query:{recipeId:312},
            body:{recipeId:312},
            userData:{userid:2,username:"raymond"}
        }
        Recipe.deleteRecipe(req, function(result){
            expect(result.result).to.eql(true);
            done();
        });  
    });

});