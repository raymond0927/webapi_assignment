var chai = require('chai');
var expect = chai.expect;
var Recipe_image = require('../controller/recipe_image_controller');
var sinon = require('sinon');

describe('recipe_image_controller', function() {
    it('getImageByRecipeid() should return the list',
        function(done) {
            var req ={
                query:{recipeId:2},
                body:{recipeId:2},
                userData:{userid:2}
            }
            Recipe_image.getImageByRecipeid(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
    });

    it('InpuChecking() should return boolean to show can be create',
    function(done) {
        var req ={
            query:{recipeId:12},
            body:{recipeId:12},
            userData:{userid:2}
        }
        Recipe_image.InpuChecking(req, function(result){
            console.log("message  "+result.message);
            expect(result.result).to.eql(true);
            done();
        });  
    });

    it('delchecking() should return boolean to show can be delete',
    function(done) {
        var req ={
            query:{recipeId:12,imageId:[12]},
            body:{recipeId:12,imageId:[12]},
            userData:{userid:2}
        }
        Recipe_image.delchecking(req, function(result){
            console.log("message  "+result.message);
            expect(result.result).to.eql(true);
            done();
        });  
    });

});