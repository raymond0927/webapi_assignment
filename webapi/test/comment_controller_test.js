var chai = require('chai');
var expect = chai.expect;
var Comment = require('../controller/comment_controller');
var sinon = require('sinon');

describe('Comment_controller', function() {
    it('InpuChecking() should return boolean to show whether the user can insert',
        function(done) {
            var req ={
                query:{recipeId:12},
                userData:{userid:2}
            }
            Comment.InpuChecking(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
    });

    it('getCommentByid() should return boolean to show whether the user can insert',
        function(done) {
            var req ={
                query:{commentid:2},
                userData:{userid:2}
            }
            Comment.getCommentByid(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
        });

        it('getCommentByRecipeId() should return result',
        function(done) {
            var req ={
                query:{recipeId:2},
                userData:{userid:2}
            }
            Comment.getCommentByRecipeId(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
        });

        it('createComment() should return boolean to show create success',
        function(done) {
            var req ={
                query:{recipeId:2},
                body:{recipeId:2,comment_eng:"test"},
                userData:{userid:2,username:"raymond"}
            }
            Comment.createComment(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
        });

        it('editComment() should return boolean to show edit success',
        function(done) {
            var req ={
                query:{commentid:2},
                body:{commentid:2},
                userData:{userid:2,username:"raymond"}
            }
            Comment.editComment(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
        });

        it('deleteComment() should return boolean to show edit success',
        function(done) {
            var req ={
                query:{commentid:642},
                body:{commentid:642},
                userData:{userid:2,username:"raymond"}
            }
            Comment.deleteComment(req, function(result){
                expect(result.result).to.eql(true);
                done();
            });  
        });

});